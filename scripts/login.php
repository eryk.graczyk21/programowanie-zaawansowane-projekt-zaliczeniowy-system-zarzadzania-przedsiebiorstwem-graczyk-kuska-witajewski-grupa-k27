<?php
    session_start();
    require_once('./connect.php');
    
    if(empty($_POST['login'])) {
        $_SESSION['error']="Podaj login!";
        echo '<script>history.back();</script>';
    } else{
        echo $_POST['login'];

        $query=$con->prepare('select id, login, password, role_id, concat(name," ",surname) as username from user where login = :login');
        $query->bindParam('login',$_POST['login'],PDO::PARAM_STR);
        $query->execute();
        if($query->rowCount()==1){
            $data=$query->fetch(PDO::FETCH_ASSOC);   
            if(password_verify($_POST['pass'],$data['password'])){
                $_SESSION['login']['isLogged']=1;
                $_SESSION['login']['login']=$data['login'];
                $_SESSION['login']['roleID']=$data['role_id'];
                $_SESSION['login']['userID']=$data['id'];
                $_SESSION['login']['username']=$data['username'];

                $insertLog=$con->prepare('INSERT INTO logs (id_user, ip, type) VALUES (:uid, :ip, 2)'); // insertuje log z poprawnym logowaniem
                $insertLog->bindParam(':uid',$data['id'],PDO::PARAM_INT);
                $insertLog->bindParam(':ip',$_SERVER['REMOTE_ADDR'],PDO::PARAM_STR);
                $insertLog->execute();

                // Zapisywanie log'ów do pliku CSV

                $csvFilePath = '../logs.csv';

                // Sprawdź, czy plik istnieje
                if (!file_exists($csvFilePath)) {
                    // Jeśli plik nie istnieje, utwórz nowy z nagłówkami kolumn
                    $csvHeader = ['id_log', 'id_user', 'ip', 'type']; // Dodaj kolumnę 'id' na początku
                    $csvFile = fopen($csvFilePath, 'a'); // Otwórz plik w trybie dołączania (append)
                    fputcsv($csvFile, $csvHeader, ';', '"');
                    fclose($csvFile);
                }

                // Pobierz aktualną ilość wpisów w pliku CSV
                $csvRows = count(file($csvFilePath)) - 1;

                // Dodaj nowy wpis do pliku CSV
                $csvData = [$csvRows + 1, $data['id'], $_SERVER['REMOTE_ADDR'], 'successlogin'];
                $csvFile = fopen($csvFilePath, 'a'); // Otwórz plik w trybie dołączania (append)
                fputcsv($csvFile, $csvData, ';', '"');
                fclose($csvFile);

                // -------------------------------

                header('Location:../dashboard.php');
                $error=0;
                exit();
            }else{
                $_SESSION['error']="Złe hasło!";

                $insertLog=$con->prepare('INSERT INTO logs (id_user, ip, type) VALUES (:uid, :ip, 1)'); //insertuje log z błedem logowania
                $insertLog->bindParam(':uid',$data['id'],PDO::PARAM_INT);
                $insertLog->bindParam(':ip',$_SERVER['REMOTE_ADDR'],PDO::PARAM_STR);
                $insertLog->execute();

                // Zapisywanie log'ów do pliku CSV

                $csvFilePath = '../logs.csv';

                // Sprawdź, czy plik istnieje
                if (!file_exists($csvFilePath)) {
                    // Jeśli plik nie istnieje, utwórz nowy z nagłówkami kolumn
                    $csvHeader = ['id_log', 'id_user', 'ip', 'type']; // Dodaj kolumnę 'id' na początku
                    $csvFile = fopen($csvFilePath, 'a'); // Otwórz plik w trybie dołączania (append)
                    fputcsv($csvFile, $csvHeader, ';', '"');
                    fclose($csvFile);
                }

                // Pobierz aktualną ilość wpisów w pliku CSV
                $csvRows = count(file($csvFilePath)) - 1;

                // Dodaj nowy wpis do pliku CSV
                $csvData = [$csvRows + 1, $data['id'], $_SERVER['REMOTE_ADDR'], 'badpassword'];
                $csvFile = fopen($csvFilePath, 'a'); // Otwórz plik w trybie dołączania (append)
                fputcsv($csvFile, $csvData, ';', '"');
                fclose($csvFile);

                // -------------------------------

                echo '<script>history.back();</script>';
            }
            
            $error=0;
        }else{
            $_SESSION['error']="Nie ma takiego użytkownika!";
            echo '<script>history.back();</script>';
        }
    }
?>