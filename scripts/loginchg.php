<?php
    session_start();
    require_once('./connect.php');
    if($_SESSION['login']['isLogged']==0){
        header('location:../index.php');
        exit();
    }
    foreach($_POST as $data){
        if(empty($data)){
            $_SESSION['error']="Wypełnij wszystkie pola!!";
            echo '<script>history.back();</script>';
            exit();
        }
    }
    if($_POST['newlogin1']!==$_POST['newlogin2']){
        $_SESSION['error']="Podane loginy nie są zgodne!";
        echo '<script>history.back();</script>';
        exit();
    }
    
    $getLogin=$con->prepare('select login from user where id=:uid');
    $getLogin->bindParam('uid',$_SESSION['login']['userID'],PDO::PARAM_INT);
    $getLogin->execute();
    $login=$getLogin->fetch(PDO::FETCH_ASSOC);

        try{
            $con->beginTransaction();
            $Login=$_POST['newlogin1'];
            $insertLogin=$con->prepare('UPDATE `user` SET login = :login WHERE `id` = :uid');
            $insertLogin->bindParam(':login',$Login,PDO::PARAM_STR);
            $insertLogin->bindParam(':uid',$_SESSION['login']['userID'],PDO::PARAM_INT);
            $insertLogin->execute();

            // Zapisywanie log'ów do pliku CSV

            $csvFilePath = '../logs.csv';

            // Sprawdź, czy plik istnieje
            if (!file_exists($csvFilePath)) {
                // Jeśli plik nie istnieje, utwórz nowy z nagłówkami kolumn
                $csvHeader = ['id', 'id_user', 'ip', 'type', 'who']; // Dodaj kolumnę 'id' na początku
                $csvFile = fopen($csvFilePath, 'a'); // Otwórz plik w trybie dołączania (append)
                fputcsv($csvFile, $csvHeader, ';', '"');
                fclose($csvFile);
            }

            // Pobierz aktualną ilość wpisów w pliku CSV
            $csvRows = count(file($csvFilePath)) - 1;

            // Dodaj nowy wpis do pliku CSV
            $csvData = [$csvRows + 1, $_SESSION['login']['userID'], $_SERVER['REMOTE_ADDR'], 'loginchg', 'user'];
            $csvFile = fopen($csvFilePath, 'a'); // Otwórz plik w trybie dołączania (append)
            fputcsv($csvFile, $csvData, ';', '"');
            fclose($csvFile);

            // -------------------------------

            $insertLogs=$con->prepare('INSERT INTO `logs` (id_user, ip, type, who) VALUES (:uid, :ip, 4, 3)');
            $insertLogs->bindParam(':uid',$_SESSION['login']['userID'],PDO::PARAM_INT);
            $insertLogs->bindParam(':ip',$_SERVER['REMOTE_ADDR'],PDO::PARAM_STR);
            $insertLogs->execute();
            $con->commit();
            $_SESSION['success']="Zmieniono login!";
            header('Location:../settings.php');

        }catch(PDOException $e){
            $con->rollback();
            echo $e->getMessage;
            $_SESSION['error']="Niezindentyfikowany błąd";
            echo '<script>history.back();</script>';
            exit();
        }
    
?>