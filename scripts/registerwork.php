<?php
    session_start();
    require_once('./connect.php');
    if($_SESSION['login']['isLogged']==0){
        header('location:../index.php');
        exit();
    }
    
    $uid=$_SESSION['login']['userID'];

    $startdate = date("Y-m-d H:i:s",strtotime($_POST['start_date']));
    $enddate = date("Y-m-d H:i:s",strtotime($_POST['end_date']));

    echo $startdate;
    echo $enddate;

    $checkExist = $con->prepare("SELECT * FROM `workhours` WHERE `start_time` in (:startdate ,:enddate) and user_id = :uid");
    $checkExist->bindParam('startdate', $startdate, PDO::PARAM_STR);
    $checkExist->bindParam('enddate', $enddate, PDO::PARAM_STR);
    $checkExist->bindParam('uid', $uid, PDO::PARAM_INT);
    $checkExist->execute();

    if($checkExist->rowCount()==1){
        $_SESSION['error']="Dodałeś już swoją obecność";
        echo '<script>history.back();</script>';
        exit();
    }
    
    $insertWorkhours=$con->prepare("INSERT INTO `workhours`(`user_id`, `created_at`, `start_time`, `end_time`) VALUES (:uid, current_timestamp(), :start_time, :end_time)");
    $insertWorkhours->bindParam('uid',$uid,PDO::PARAM_INT);
    $insertWorkhours->bindParam('start_time',$startdate,PDO::PARAM_STR);
    $insertWorkhours->bindParam('end_time',$enddate,PDO::PARAM_STR);
    $insertWorkhours->execute();

    $_SESSION['success'] = "Pomyślnie dodano wydarzenie czasowe!";
    header('Location:../workhours.php');

?>



    



    
 
       
    
?>