<?php
    session_start();
    require_once('./connect.php');
    if(($_SESSION['login']['isLogged']==0)||($_SESSION['login']['roleID']==3)){// tylko admin lub manager moga wejść
        echo '<script>history.back();</script>';
        exit();
    }

    if(empty($_GET['workhours_id'])){
        echo '<script>history.back();</script>';
        exit();
    }
    
    $uid=$_SESSION['login']['userID'];
    
    $queryworkhours=$con->prepare('select workhours.id, workhours.user_id from workhours join user on user.id=workhours.user_id where workhours.id=:wid');
    $queryworkhours->bindParam('wid',$_GET['workhours_id'],PDO::PARAM_INT);
    $queryworkhours->execute();
    $getworkhours=$queryworkhours->fetch(PDO::FETCH_ASSOC);
    if($_SESSION['login']['roleID']==1){
        $acceptworkhours=$con->prepare('UPDATE workhours SET is_accepted = 1 WHERE id=:wid');
        $acceptworkhours->bindParam('wid',$_GET['workhours_id'],PDO::PARAM_INT);
        $acceptworkhours->execute();
        header('Location:../manageworkhours.php');
    }
?>