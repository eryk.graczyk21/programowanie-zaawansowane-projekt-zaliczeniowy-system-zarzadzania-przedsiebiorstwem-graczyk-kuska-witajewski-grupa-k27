-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 20 Sty 2024, 09:07
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `projekt_wsb`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `type` enum('badpassword','successlogin','passwordchg','loginchg','') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `who` enum('','admin','user') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `logs`
--

INSERT INTO `logs` (`id`, `id_user`, `ip`, `type`, `created_at`, `who`) VALUES
(70, 0, '::1', 'badpassword', '2024-01-13 16:53:23', ''),
(71, 0, '::1', 'badpassword', '2024-01-13 16:53:28', ''),
(72, 0, '::1', 'badpassword', '2024-01-13 16:53:33', ''),
(73, 0, '::1', 'badpassword', '2024-01-13 16:53:42', ''),
(74, 0, '::1', 'badpassword', '2024-01-13 16:54:28', ''),
(75, 0, '::1', 'successlogin', '2024-01-13 16:54:33', ''),
(76, 2, '::1', 'badpassword', '2024-01-13 16:56:35', ''),
(77, 0, '::1', 'successlogin', '2024-01-13 16:56:46', ''),
(78, 6, '::1', 'passwordchg', '2024-01-13 17:13:25', 'admin'),
(79, 6, '::1', 'successlogin', '2024-01-13 17:13:48', ''),
(80, 0, '::1', 'badpassword', '2024-01-13 17:14:12', ''),
(81, 0, '::1', 'successlogin', '2024-01-13 17:14:17', ''),
(82, 6, '::1', 'successlogin', '2024-01-13 17:14:32', ''),
(83, 0, '::1', 'successlogin', '2024-01-13 17:23:48', ''),
(84, 2, '::1', 'badpassword', '2024-01-13 17:36:19', ''),
(85, 6, '::1', 'badpassword', '2024-01-13 17:36:29', ''),
(86, 6, '::1', 'successlogin', '2024-01-13 17:36:36', ''),
(87, 0, '::1', 'successlogin', '2024-01-17 18:28:48', ''),
(88, 2, '::1', 'badpassword', '2024-01-17 18:35:57', ''),
(89, 2, '::1', 'badpassword', '2024-01-17 18:36:01', ''),
(90, 2, '::1', 'badpassword', '2024-01-17 18:36:05', ''),
(91, 0, '::1', 'successlogin', '2024-01-17 18:36:10', ''),
(92, 2, '::1', 'passwordchg', '2024-01-17 18:36:37', 'admin'),
(93, 7, '::1', 'badpassword', '2024-01-17 18:36:51', ''),
(94, 7, '::1', 'badpassword', '2024-01-17 18:36:55', ''),
(95, 0, '::1', 'successlogin', '2024-01-17 18:37:02', ''),
(96, 7, '::1', 'passwordchg', '2024-01-17 18:37:26', 'admin'),
(97, 7, '::1', 'badpassword', '2024-01-17 18:37:39', ''),
(98, 7, '::1', 'successlogin', '2024-01-17 18:37:47', ''),
(99, 0, '::1', 'successlogin', '2024-01-17 18:38:34', ''),
(100, 7, '::1', 'badpassword', '2024-01-17 18:43:47', ''),
(101, 7, '::1', 'successlogin', '2024-01-17 18:43:51', ''),
(102, 2, '::1', 'badpassword', '2024-01-18 13:22:51', ''),
(103, 2, '::1', 'badpassword', '2024-01-18 13:23:03', ''),
(104, 2, '::1', 'badpassword', '2024-01-18 13:23:08', ''),
(105, 0, '::1', 'badpassword', '2024-01-18 13:26:32', ''),
(106, 2, '::1', 'badpassword', '2024-01-18 13:27:07', ''),
(107, 0, '::1', 'successlogin', '2024-01-18 13:31:21', ''),
(108, 1, '::1', 'successlogin', '2024-01-18 13:35:12', ''),
(109, 0, '::1', 'successlogin', '2024-01-18 13:35:52', ''),
(110, 2, '::1', 'badpassword', '2024-01-18 15:06:46', ''),
(111, 2, '::1', 'badpassword', '2024-01-18 15:06:52', ''),
(112, 1, '::1', 'successlogin', '2024-01-18 15:07:12', ''),
(113, 6, '::1', 'badpassword', '2024-01-18 15:08:51', ''),
(114, 0, '::1', 'successlogin', '2024-01-18 15:09:05', ''),
(115, 2, '::1', 'badpassword', '2024-01-18 15:09:15', ''),
(116, 2, '::1', 'badpassword', '2024-01-18 15:09:19', ''),
(117, 2, '::1', 'badpassword', '2024-01-18 15:09:22', ''),
(118, 2, '::1', 'badpassword', '2024-01-18 15:09:27', ''),
(119, 2, '::1', 'badpassword', '2024-01-18 15:09:33', ''),
(120, 0, '::1', 'badpassword', '2024-01-18 15:09:40', ''),
(121, 0, '::1', 'successlogin', '2024-01-18 15:09:44', ''),
(122, 2, '::1', 'passwordchg', '2024-01-18 15:09:56', 'admin'),
(123, 2, '::1', 'successlogin', '2024-01-18 15:10:04', ''),
(124, 0, '::1', 'successlogin', '2024-01-18 15:21:04', ''),
(125, 0, '::1', 'successlogin', '2024-01-18 15:22:50', ''),
(126, 1, '::1', 'successlogin', '2024-01-18 15:27:47', ''),
(127, 0, '::1', 'successlogin', '2024-01-18 15:27:56', ''),
(128, 1, '::1', 'badpassword', '2024-01-18 15:48:53', ''),
(129, 1, '::1', 'successlogin', '2024-01-18 15:48:57', ''),
(130, 2, '::1', 'successlogin', '2024-01-18 15:56:22', ''),
(131, 0, '::1', 'badpassword', '2024-01-18 15:57:28', ''),
(132, 0, '::1', 'successlogin', '2024-01-18 15:57:31', ''),
(133, 0, '::1', 'successlogin', '2024-01-18 19:18:20', ''),
(134, 2, '::1', 'successlogin', '2024-01-18 19:21:57', ''),
(135, 1, '::1', 'successlogin', '2024-01-18 19:24:23', ''),
(136, 0, '::1', 'badpassword', '2024-01-18 19:26:34', ''),
(137, 0, '::1', 'successlogin', '2024-01-18 19:26:37', ''),
(138, 1, '::1', 'successlogin', '2024-01-18 19:36:04', ''),
(139, 2, '::1', 'successlogin', '2024-01-18 19:39:35', ''),
(140, 0, '::1', 'successlogin', '2024-01-18 19:40:16', ''),
(141, 2, '::1', 'successlogin', '2024-01-18 19:42:36', ''),
(142, 0, '::1', 'successlogin', '2024-01-18 19:52:15', ''),
(143, 2, '::1', 'successlogin', '2024-01-18 19:55:02', ''),
(144, 0, '::1', 'successlogin', '2024-01-18 19:55:45', ''),
(145, 1, '::1', 'successlogin', '2024-01-18 19:55:55', ''),
(146, 0, '::1', 'successlogin', '2024-01-18 19:56:08', ''),
(147, 0, '::1', 'successlogin', '2024-01-19 12:12:13', ''),
(148, 1, '::1', 'passwordchg', '2024-01-19 12:12:26', 'admin'),
(149, 2, '::1', 'passwordchg', '2024-01-19 12:12:37', 'admin'),
(150, 6, '::1', 'passwordchg', '2024-01-19 12:12:51', 'admin'),
(151, 7, '::1', 'passwordchg', '2024-01-19 12:13:05', 'admin'),
(152, 8, '::1', 'passwordchg', '2024-01-19 12:13:15', 'admin'),
(153, 1, '::1', 'successlogin', '2024-01-19 12:30:12', ''),
(154, 1, '::1', 'passwordchg', '2024-01-19 12:38:29', 'user'),
(155, 1, '::1', 'passwordchg', '2024-01-19 12:39:23', 'user'),
(156, 1, '::1', 'passwordchg', '2024-01-19 12:40:12', 'user'),
(157, 1, '::1', 'passwordchg', '2024-01-19 12:41:33', 'user'),
(158, 1, '::1', 'passwordchg', '2024-01-19 12:42:53', 'user'),
(159, 1, '::1', 'successlogin', '2024-01-19 12:43:09', ''),
(160, 0, '::1', 'successlogin', '2024-01-19 12:43:25', ''),
(161, 0, '::1', 'successlogin', '2024-01-19 12:45:16', ''),
(162, 1, '::1', 'successlogin', '2024-01-19 12:52:14', ''),
(163, 1, '::1', 'loginchg', '2024-01-19 12:52:23', 'user'),
(164, 1, '::1', 'successlogin', '2024-01-19 12:52:30', ''),
(165, 1, '::1', 'loginchg', '2024-01-19 12:55:01', 'user'),
(166, 1, '::1', 'loginchg', '2024-01-19 12:55:43', 'user'),
(167, 0, '::1', 'successlogin', '2024-01-19 13:10:54', ''),
(168, 0, '::1', 'successlogin', '2024-01-20 07:59:12', ''),
(169, 1, '::1', 'successlogin', '2024-01-20 08:04:06', ''),
(170, 1, '::1', 'successlogin', '2024-01-20 08:04:22', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` enum('admin','manager','user','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'admin'),
(2, 'manager'),
(3, 'user');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `birthday` date NOT NULL,
  `manager_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `password` varchar(250) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `login`, `name`, `surname`, `email`, `birthday`, `manager_id`, `created_at`, `password`, `role_id`) VALUES
(0, 'admin', 'admin', 'admin', 'admin@admin.com', '2023-05-23', 0, '2023-05-23 16:52:28', '$argon2id$v=19$m=16,t=2,p=1$YWNhM1BjRG9nQmwyeUJ1Wg$kFIHvr3vIhEApKsWPVOIFQ', 1),
(1, 'mmarecki', 'Marek', 'Marecki', 'marek.marecki@corp.com', '1993-03-02', 2, '2023-05-28 14:57:01', '$argon2id$v=19$m=65536,t=4,p=1$T3VhTTZOMDFEQ3pUSnhoYw$2QNQoX+CJFP3VcOL7oBwkht4qX08UyHi9F/hhgpXw6g', 3),
(2, 'jjanecki', 'Jan', 'Janecki', 'jan.janecki@corp.com', '1970-05-15', 0, '2023-05-28 15:08:31', '$argon2id$v=19$m=65536,t=4,p=1$YW4zbGdwWk4zRE1QakhXNw$CTNaowf1vIs0VV7nWYd9TDuVQFJYhpEaoqvt8I0gCC0', 2),
(6, 'ppiotrowski', 'Piotr', 'Piotrowski', 'piotr.piotrowski@corp.com', '1996-05-15', 0, '2023-06-09 22:03:49', '$argon2id$v=19$m=65536,t=4,p=1$b3UuejlRYlhUL1VyL1MvVg$+TiB0ROKIh0G94+Zps1BAjBZuTdclC0nkSsfqqYap3s', 2),
(7, 'jjanuszewski', 'Janusz', 'Januszewski', 'janusz.januszewski@corp.pl', '1984-02-12', 6, '2023-06-11 18:23:57', '$argon2id$v=19$m=65536,t=4,p=1$VXI3ZWk5RllSSXJ6dmJoSg$GK7mcyTr8X1IqHN8EfKjGukFleUQJxDVU/vpMzmHMm8', 3),
(8, 'aadamczak', 'Adam', 'Adamczak', 'adam.adamczak@corp.com', '1945-11-11', 0, '2023-06-16 19:38:34', '$argon2id$v=19$m=65536,t=4,p=1$UGpMMm5samtuUlNOSzczaQ$7UGRXdZw813PdbCwnNPxH5J7gSaeobvGxRv7ifDjpd4', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `vacation`
--

CREATE TABLE `vacation` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_accepted` int(11) NOT NULL DEFAULT 0,
  `message` varchar(250) NOT NULL,
  `accepted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `vacation`
--

INSERT INTO `vacation` (`id`, `user_id`, `created_at`, `start_date`, `end_date`, `is_accepted`, `message`, `accepted_by`) VALUES
(10, 6, '2024-01-13 17:14:02', '2024-01-17', '2024-01-26', 1, '', 0),
(11, 2, '2024-01-18 19:22:39', '2024-01-22', '2024-01-26', 0, 'Plsss', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `workhours`
--

CREATE TABLE `workhours` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `is_accepted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `workhours`
--

INSERT INTO `workhours` (`id`, `user_id`, `created_at`, `start_time`, `end_time`, `is_accepted`) VALUES
(8, 1, '2024-01-18 15:55:43', '2024-01-18 07:00:00', '2024-01-18 15:00:00', 1),
(9, 2, '2024-01-18 19:23:41', '2024-01-18 07:00:00', '2024-01-18 15:00:00', NULL),
(10, 1, '2024-01-20 08:04:31', '2024-01-20 07:00:00', '2024-01-20 15:00:00', NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeksy dla tabeli `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `manager_id` (`manager_id`);

--
-- Indeksy dla tabeli `vacation`
--
ALTER TABLE `vacation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `accepted_by` (`accepted_by`);

--
-- Indeksy dla tabeli `workhours`
--
ALTER TABLE `workhours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT dla tabeli `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `vacation`
--
ALTER TABLE `vacation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT dla tabeli `workhours`
--
ALTER TABLE `workhours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Ograniczenia dla tabeli `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Ograniczenia dla tabeli `vacation`
--
ALTER TABLE `vacation`
  ADD CONSTRAINT `vacation_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `vacation_ibfk_2` FOREIGN KEY (`accepted_by`) REFERENCES `user` (`id`);

--
-- Ograniczenia dla tabeli `workhours`
--
ALTER TABLE `workhours`
  ADD CONSTRAINT `workhours_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
