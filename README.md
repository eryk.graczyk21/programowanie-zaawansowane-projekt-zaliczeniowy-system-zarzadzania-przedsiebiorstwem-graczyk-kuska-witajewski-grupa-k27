Instalacja projektu na przykładzie programu xampp.

1. W programie xampp uruchamiamy serwer Apache i MySQL.
2. W lokalizacji "C:\xampp\htdocs\dashboard" tworzymy folder o nazwie "projekt" i kopiujemy do niego wszystkie pliki z archiwum.
3. Na serwerze MySQL tworzymy bazę danych o nazwie "projekt_wsb" czyli tak samo, jak nazywa się dołączony w archiwum ZIP plik z rozszerzeniem ".sql" z bazą danych.
4. Do stworzonej bazy improtujemy zawartośc pliku "projekt_wbs.sql".
5. W przeglądarce przechodzimy na adres "http://localhost/dashboard/projekt/". *

*Już zainstalowany projekt dostępny również pod adresem "https://www.projektwsb.erykgraczyk.pl/".

Miłego sprawdzania! :)

Eryk Graczyk, Wojciech Kuska, Maciej Witajewski
Grupa K27
